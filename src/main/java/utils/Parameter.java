package utils;

import org.testng.annotations.Test;
import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import week4day2.SeMethods;

public class Parameter extends SeMethods {
	
	public String dataSheetName;
	//@BeforeMethod
	
	//@Parameters({"url", "username","password"})
	public void login(String url , String username , String password)
	
	{
		startApp("chrome",url);
		WebElement element1 = locateElement("username");
		type(element1,username);
		WebElement pass1 = locateElement("password");
		type(pass1, password);
		WebElement clk = locateElement("class", "decorativeSubmit");
		click(clk);
		WebElement lt1 = locateElement("linktext", "CRM/SFA");
		click(lt1);
		WebElement lt2 = locateElement("linktext", "Create Lead");
		click(lt2);
		
	}
	
	
	//@Test(dataProvider="qa")
	
	public void lead(String cName , String fName , String lName)

	{
		WebElement element2 = locateElement("createLeadForm_companyName");
		type(element2, cName);
		WebElement element3 = locateElement("createLeadForm_firstName");
		type(element3, fName);
		WebElement element4 = locateElement("createLeadForm_lastName");
		type(element4, lName);
		WebElement element5 = locateElement("class", "smallSubmit");
		click(element5);
		verifyTitle("Leaftaps Login");	
		
	}
	@DataProvider(name ="qa", indices= {1})
	public Object[][] dp() throws IOException
	
	{
		
		Object[][] data = LearnExcel.excel(dataSheetName);
		return data;
		/*Object[][] data= new String [2][3];
		data[0][0]= "CTS";
		data[0][1]= "Bharathi";
		data[0][2]= "P";
		

		data[1][0]= "IBM";
		data[1][1]= "Anandha GAnesh";
		data[1][2]= "A";*/

		//return data;
		
	}
}


