package utils;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import week4day2.ProjectMethods;

public class SampleCreateLead extends ProjectMethods  {

	//@Test
	public void CreateLead()
	
	{
		
		System.out.println("Executing Before Method");
	    WebElement lt1 = locateElement("linktext", "CRM/SFA");
		click(lt1);
		WebElement lt2 = locateElement("linktext", "Create Lead");
		click(lt2);
		WebElement element2 = locateElement("createLeadForm_companyName");
		type(element2, "IBM");
		WebElement element3 = locateElement("createLeadForm_firstName");
		type(element3, "Anandha");
		WebElement element4 = locateElement("createLeadForm_lastName");
		type(element4, "Ganesh");
		WebElement element5 = locateElement("class", "smallSubmit");
		click(element5);

	}
	
}
