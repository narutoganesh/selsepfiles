package weekday3day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Irctc {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		List<WebElement> link = driver.findElementsByTagName("a");
		int size = link.size();
		System.out.println("The total number of hyperlinks are"+size);
		link.get(140).click();
	
	}
	
}
