package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import week4day2.ProjectMethods;

public class TC001_LoginLogout extends ProjectMethods {
//@BeforeTest
	
public void setData() 

{
		
	dataSheetName="TC001";	
		
	}
		
	
//@Test(dataProvider ="qa")
public void loginlogout(String uName, String password)
{
	
 new LoginPage().enterUsername(uName).enterPassword(password).clickLogin().clickLogout();
 
	
}

}
