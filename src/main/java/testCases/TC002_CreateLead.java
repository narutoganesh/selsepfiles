package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import week4day2.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
@BeforeTest
	
public void setData() 

{
		
	dataSheetName="createLead";	
		
	}
		
	
@Test(dataProvider ="qa")
public void CreateLead(String uName , String password, String companyName, String firstName ,String lastName)
{
	
new LoginPage().enterUsername(uName).enterPassword(password).clickLogin().clickCRM().clickLeads()
.clickCreateLead().entercname(companyName).enterfname(firstName).enterlname(lastName).clickCreateLeadButton();
	
}

}
